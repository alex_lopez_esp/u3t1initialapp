package dam.alex.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.view.View.OnClickListener;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private int count;
    private TextView tvDisplay;
    private Button buttonIncrease,buttonDecrease,buttonReset,buttonIncrease2,buttonDecrease2;
    private static final String SAVE="errorkey";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();//TODO Llamamos al metodo SetUI
    }
    private void setUI(){
        tvDisplay=findViewById(R.id.tvDisplay);//TODO Creamos el boton con la id
        buttonIncrease=findViewById(R.id.buttonIncrease);//TODO Creamos el boton con la id
        buttonDecrease=findViewById(R.id.buttonDecrease);//TODO Creamos el boton con la id
        buttonReset=findViewById(R.id.buttonReset);//TODO Creamos el boton con la id
        buttonIncrease2=findViewById(R.id.buttonIncrease2);//TODO Creamos el boton con la id
        buttonDecrease2=findViewById(R.id.buttonDecrease2);//TODO Creamos el boton con la id

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonIncrease:count++;break;//TODO Si se le da al  boton de Increase el count incrementa en uno
            case R.id.buttonDecrease:count--;break ;//TODO Si se le da al boton de Decrease el count lo que hace es decrementar en uno
            case R.id.buttonReset:count=0;break;//TODO Lo que hace este boton es que cuando se le presiona el contador se resetea y se pone en 0
            case R.id.buttonIncrease2:count=count+2;break;//TODO Cuando pulsamos el voton de Increase2 el count incrementa en 2
            case R.id.buttonDecrease2:count=count-2;break;//TODO Si se le da al boton de Decrease2 el count lo que hace es decrementar en dos
        }

        tvDisplay.setText(getString(R.string.number_of_elements)+": "+count);//TODO Muestra el resultado de los count en el TextView
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(SAVE,tvDisplay.getText().toString());//TODO Guarla la información que contiene el textView en un atributo
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
       // tvDisplay.setText(tvDisplay.getText());//TODO Cuando se restaura seteamos el texto que tenia el textView
        super.onRestoreInstanceState(savedInstanceState);
        tvDisplay.setText(savedInstanceState.getString(SAVE));//TODO Seteamos el TextView con el contenido que se ha guardado en el argumento
    }
}